from decouple import config
from kafka import KafkaConsumer, KafkaProducer
import concurrent.futures
from gitlabx import factories
from pprint import pprint
import json

consumer = KafkaConsumer(
    config('TOPIC'),
    group_id=config('GROUP_ID'),
    bootstrap_servers=config('SERVERS'),
    auto_offset_reset='earliest',  
    enable_auto_commit=True,  
    value_deserializer=lambda x: json.loads(x.decode('utf-8'))
    )

def publish (**kwargs):
    pprint ("topic {}".format(kwargs["topic"]))
    pprint ("-"*50)
    
    producer = KafkaProducer(bootstrap_servers=config('SERVERS'),value_serializer=lambda v: json.dumps(v).encode('utf-8'))
    producer.send(kwargs["topic"], kwargs["data"])
    producer.flush()

def extract_function(project,extract):
    for key, value in extract.items():
        print ("{} {}".format(project['name'], key))
        value.get_by_project_function(project["id"],function=publish, topic="application.{}.{}".format("gitlab",key), extra_data = None)       


try:
    for message in consumer:
        
        mensagem = message.value
        
        personal_access_token  = mensagem["secret"]
        gitlab_url = mensagem["gitlab_url"]

        project_factory = factories.ProjectFactory(personal_access_token=personal_access_token, gitlab_url=gitlab_url)
        projects = project_factory.get_all_function(today=False,function=publish, topic="application.{}.{}".format("gitlab","project"), extra_data = None)  
        
        extract = {
            
            "branch"                : factories.BranchesFactory(personal_access_token=personal_access_token, gitlab_url=gitlab_url),
            "member"                : factories.MembersFactory(personal_access_token=personal_access_token, gitlab_url=gitlab_url),
            "commit"                : factories.CommitsFactory(personal_access_token=personal_access_token, gitlab_url=gitlab_url),
            "jobs"                  : factories.JobFactory(personal_access_token=personal_access_token, gitlab_url=gitlab_url),
            "pipeline"              : factories.PipelineFactory(personal_access_token=personal_access_token, gitlab_url=gitlab_url),
            "pipelinesschedules"    : factories.PipelinesSchedulesFactory(personal_access_token=personal_access_token, gitlab_url=gitlab_url),
            "mergerequest"          : factories.MergeRequestFactory(personal_access_token=personal_access_token, gitlab_url=gitlab_url),
            "deployments"           : factories.DeploymentsFactory(personal_access_token=personal_access_token, gitlab_url=gitlab_url),
        }

        with concurrent.futures.ProcessPoolExecutor(max_workers=100) as executor:
            list(executor.map(extract_function, projects,  [extract]*len(projects)))


except KeyboardInterrupt:
    print ("Interrupção do teclado detectada. Encerrando o consumidor");
finally:
    consumer.close()