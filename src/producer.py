from kafka import KafkaProducer
from decouple import config
import json
producer = KafkaProducer(bootstrap_servers=config('SERVERS'),value_serializer=lambda v: json.dumps(v).encode('utf-8'))
data = {  
        "secret":config('SECRET_TEST'),
        "gitlab_url": config('URL_TEST'),
        "configuration_uuid": config('CONFIGURATION_UUID_TEST'),
        "organization_uuid": config('ORGANIZATION_UUID__TEST'),
        "organization_name": config('ORGANIZATION_NAME')
        }
producer.send(config('TOPIC'), data)
producer.flush()
print ("send - {} {}".format(config('SERVERS'),config('TOPIC')))
print (data)
