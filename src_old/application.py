import apache_beam as beam
from beam_nuggets.io import kafkaio
from decouple import config
from services.transform import TransformPublish,TransformByProjectEntity, TransformRetrieveProject
import logging
from apache_beam.options.pipeline_options import PipelineOptions
logging.basicConfig(level=logging.INFO)


consumer_config = {"topic": config('TOPIC'),
                   "bootstrap_servers": config('SERVERS'),
                   "group_id": config('GROUP_ID')}


def run_pipeline():
    pipeline_options = PipelineOptions(
        runner="DirectRunner",  # Or other runner like "DataflowRunner"
        num_workers=10  # Specify the desired number of workers
    )

    with beam.Pipeline(options=pipeline_options) as pipeline:


        credential = (pipeline|  "Reading messages from Kafka"  >> kafkaio.KafkaConsume(
                                            consumer_config=consumer_config))

        projects = credential | "Extract Data from atomic entities from GitLab and Send To Kafka" >> TransformRetrieveProject() 
        
        projects | "Publish Project Information" >> TransformPublish(entity="project") 

        projects | "Extract Data from project dependency entites GitLab and Send To Kafka: member" >> TransformByProjectEntity('member')
        
        projects | "Extract Data from project dependency entites GitLab and Send To Kafka: branches" >> TransformByProjectEntity('branches')

        projects | "Extract Data from project dependency entites GitLab and Send To Kafka: commits" >> TransformByProjectEntity('commits')
        
        projects | "Extract Data from project dependency entites GitLab and Send To Kafka: repositorytree" >> TransformByProjectEntity('repositorytree')
        
        projects | "Extract Data from project dependency entites GitLab and Send To Kafka: commits and Files" >> TransformByProjectEntity('commits', details = True)
        
        

        



if __name__ == "__main__":
    run_pipeline()   


    