import apache_beam as beam
from pprint import pprint
from .extract import Extract
import json
import uuid 
from .util import Util


class FnExtractOnlyProject(beam.DoFn):
   """ Abstract Function"""
   def __init__(self) -> None:
      super().__init__()
      self.application_entity = Extract()
      self.util = Util()
      self.function_name = 'project'
      
      
   def process(self, element):

      try:
       
         data = json.loads(element[1])
         
         self.application_entity.config (
            entity = self.function_name,
            personal_access_token = data['secret'],
            gitlab_url = data['gitlab_url'],
            organization_uuid = data['configuration_uuid'],
            configuration_uuid = data['organization_uuid']
         )
      
         result = self.application_entity.do()
         return result
      except Exception as e:
            print (e)


class FnExtractByProject(beam.DoFn):
   """ Abstract Function"""
   def __init__(self,function_name, details = False) -> None:
      super().__init__()
      self.application_entity = Extract()
      self.util = Util()
      self.details =  details
      self.function_name = function_name
      
      
   def process(self, element):

      try:
         
         self.application_entity.config (
            entity = self.function_name,
            personal_access_token = element['secret'],
            gitlab_url = element['gitlab_url'],
            organization_uuid = element['configuration_uuid'],
            configuration_uuid = element['organization_uuid']
         )
         result = self.application_entity.do_by_project(element['id'],self.details)   
         return result
      except Exception as e:
            print (e)


class FnExtract(beam.DoFn):
   
   """ Abstract Function"""
   def __init__(self,function_name) -> None:
      super().__init__()
      self.application_entity = Extract()
      self.util = Util()
      self.function_name = function_name
      
      
   def process(self, element):

      try:
       
         data = json.loads(element[1])
         
         self.application_entity.config (
            entity = self.function_name,
            personal_access_token = data['secret'],
            gitlab_url = data['gitlab_url'],
            organization_uuid = data['configuration_uuid'],
            configuration_uuid = data['organization_uuid']
         )
      
         result = self.application_entity.do()      
         return result
      except Exception as e:
            print (e)

   
           
class FnTuple(beam.DoFn):

   def __init__(self) -> None:

      super().__init__()
      self.util = Util()
      
   def process(self, element):
      
      index = str(uuid.uuid4())
      element = json.dumps (element)
      
      return [(index,element)]
      
            
      
      