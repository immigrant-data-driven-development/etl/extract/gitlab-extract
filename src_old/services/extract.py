from gitlabx import factories
import logging
from .util import Util
import uuid
logging.basicConfig(level=logging.INFO)

class Extract():
    """Abstract Class with the main function used to extract data application and save in a MongoDB"""

    def __init__(self):

        self.util = Util()
        self.instance = None
      
        
    def config (self, entity, personal_access_token, gitlab_url, organization_uuid,configuration_uuid) :

        
        self.entity = entity
        self.personal_access_token = personal_access_token
        self.gitlab_url = gitlab_url
        self.organization_uuid = organization_uuid
        self.configuration_uuid = configuration_uuid
        
        self.extract = {
            
            'project': factories.ProjectFactory(personal_access_token=self.personal_access_token,
                                                        gitlab_url=self.gitlab_url),

                   
            
            'branches': factories.BranchesFactory(personal_access_token=self.personal_access_token,
                                                            gitlab_url=self.gitlab_url),  

            'commits': factories.CommitsFactory(personal_access_token=self.personal_access_token,
                                                            gitlab_url=self.gitlab_url),  
                                                            
            'member': factories.MembersFactory(personal_access_token=self.personal_access_token,
                                                            gitlab_url=self.gitlab_url),  
                       
            
            'deployments': factories.DeploymentsFactory(personal_access_token=self.personal_access_token,
                                                            gitlab_url=self.gitlab_url),

            'events': factories.EventsFactory(personal_access_token=self.personal_access_token,
                                                            gitlab_url=self.gitlab_url),  

            'issues': factories.IssuesFactory(personal_access_token=self.personal_access_token,
                                                            gitlab_url=self.gitlab_url),  

            'repositories': factories.RepositoriesFactory(personal_access_token=self.personal_access_token,
                                                            gitlab_url=self.gitlab_url),

            'repositorytree': factories.RepositoryTreeFactory(personal_access_token=self.personal_access_token,
                                                            gitlab_url=self.gitlab_url),
                
            'projectlanguages': factories.ProjectLanguagesFactory(personal_access_token=self.personal_access_token,
                                                            gitlab_url=self.gitlab_url)
        }

        self.instance = self.extract[entity]


    def do_by_project(self, project_id, details = False):
        """Main function to retrieve and save data in a MongoDB's collection
        
        Args:
            dict data: credentials (secret, url etc) to connect a application
        
        """
        try:
            logging.info("Start Retrieve Information")
            data_extracted =  None

            if self.entity == 'commits' and details:      
                data_extracted = self.instance.get_by_project_files(project_id)      
            else:
                data_extracted = self.instance.get_by_project(project_id)  
            
            data_transformed = []
            for data in data_extracted:
                
                data_dict = self.util.object_to_dict(data)
                data_dict['entity'] = self.entity
                data_dict['configuration_uuid'] =  self.configuration_uuid 
                data_dict['organization_uuid'] =  self.organization_uuid 
                data_dict['secret'] =  self.personal_access_token
                data_dict['gitlab_url'] = self.gitlab_url 
                data_dict['internal_uuid'] = str(uuid.uuid4())
                data_transformed.append (data_dict)
            
            logging.info("End Returning")
            
            return data_transformed
            
        except Exception as e: 
            logging.error("OS error: {0}".format(e))
            logging.error(e.__dict__)  

    def do(self):
        """Main function to retrieve and save data in a MongoDB's collection
        
        Args:
            dict data: credentials (secret, url etc) to connect a application
        
        """
        try:
            logging.info("Start Retrieve Information")
            data_extracted =  None
            data_extracted = self.instance.get_all(today=False)  
            data_transformed = []
            for data in data_extracted:
                
                data_dict = self.util.object_to_dict(data)
                data_dict['entity'] = self.entity
                data_dict['configuration_uuid'] =  self.configuration_uuid 
                data_dict['organization_uuid'] =  self.organization_uuid 
                data_dict['secret'] =  self.personal_access_token
                data_dict['gitlab_url'] = self.gitlab_url 
                data_dict['internal_uuid'] = str(uuid.uuid4())
                data_transformed.append (data_dict)
            
            logging.info("End Returning")
            
            return data_transformed
            
        except Exception as e: 
            logging.error("OS error: {0}".format(e))
            logging.error(e.__dict__)    


    def __json_default(self,value):
        """ Convert a specific object to dict 
        
        Args:
            object value: a object to convert to dict
        """
        if isinstance(value, datetime.date):
            return dict(year=value.year, month=value.month, day=value.day)
        else:
            return value.__dict__

    def object_to_dict(self, entity):
        """ Convert an object to dict 
        
        Args:
            object entity: a entity to convert to dict
        """
        json_entity = json.dumps(entity,ensure_ascii=False,default = lambda o: self.__json_default(o), sort_keys=True, indent=4).encode('utf-8')
        return json.loads(json_entity)
    